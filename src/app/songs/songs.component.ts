import { Component, OnInit } from '@angular/core';
import { Song } from '../song';
import { SONGS } from '../songlist';

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.scss']
})
export class SongsComponent implements OnInit {
  songs: Song[] = [];
  byLang = 'en';
  selectedSong: Song;

  constructor() { this.reset(); }

  ngOnInit() {}

  selectSong(song: Song): void {
    this.selectedSong = song;
  }

  clearSong() {
    this.selectedSong = null;
    console.log('attempting to clear song');
  }

  selectLang(lang: string) {
    this.byLang = lang;
    console.log(this.byLang);
  }
  reset() { this.songs = SONGS.slice(); }

}
