import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SongsComponent } from './songs/songs.component';
import { AppRoutingModule } from './/app-routing.module';
import { HelloComponent } from './hello/hello.component';
import { VideoComponent } from './video/video.component';
import { YoutubePipe } from './youtube.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SongsComponent,
    HelloComponent,
    VideoComponent,
    YoutubePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
