export class Song {
  id: string;
  title: string;
  artist: string;
  url: string;
  lang: string;
  length: number;
}