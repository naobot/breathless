import { Component, EventEmitter, Input, Output, OnInit, OnDestroy } from '@angular/core';
import { timer } from 'rxjs';

import { Song } from '../song';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent {
  @Input('selectedSong') selectedSong: Song;

  @Output() dismissVideo: EventEmitter<any> = new EventEmitter();

  constructor() { }

  // Hack-y solution to automatically closing the video component
  // Instead of using actual callbacks from YouTube API,
  // the length of the video is provided in songlist db,
  // we use length and manually count down with a timer,
  // with a bit of padding added for load time, etc.

  ngOnInit() {
    console.log('song begun');
    const source = timer(this.selectedSong.length * 1000 + 20000);
    source.subscribe(fin => this.closeVideo());
  }

  closeVideo() {
    this.dismissVideo.emit();
    console.log('attempting to close video');
  }

}
